#version 330 core

// Input data
layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aColor;
layout(location = 2) in vec2 aTextureCoord;

// Output data
out vec2 textureCoordinate;
out vec4 vertexColor;

// Uniforms
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
    gl_Position = projection * view * model * vec4(aPosition, 1.0f);
    vertexColor = vec4(aColor, 1.0f);
    textureCoordinate = aTextureCoord;
}
