#version 330 core

// Input data
in vec4 vertexColor;
in vec2 textureCoordinate;

// Ouput data
out vec4 fragmentColor;

// Texture samplers
uniform sampler2D texture0;

void main() {
//     fragmentColor = vertexColor * texture(texture0, textureCoordinate);
    vec4 texColor = vertexColor * texture(texture0, textureCoordinate);
    if (texColor.a < 0.1)
        discard;
    fragmentColor = texColor;
}
