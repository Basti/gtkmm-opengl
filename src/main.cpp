/*****************************************************************************
 * <GTKmm_OpenGL - Example of GTKmm window with OpenGL.>
 * Copyright (C) 2018  B. Véron
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <gtkmm/application.h>
#include "myglarea.h"


int main (int argc, char *argv[]) {
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.opengl.example");
    
    MyGLArea win;
    win.show();
    return app->run(win);
}


/*! \mainpage
 * GTKmm_OpenGL is a simple graphical interface showing how to use OpenGL with GTKmm.\n
 * \n
 * \image html ./screenshot.png "Snapshot of the application"
 * \image latex ./screenshot.png "Snapshot of the application"
 */
