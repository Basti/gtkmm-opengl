/*****************************************************************************
 * <GTKmm_OpenGL - Example of GTKmm window with OpenGL.>
 * Copyright (C) 2018  B. Véron
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef MY_GLAREA_H
#define MY_GLAREA_H

// Local includes
#include "shader.h"

// GTKmm includes
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/glarea.h>
#include <gtkmm/button.h>
#include <gtkmm/scale.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/label.h>

// Epoxy includes (link to OpenGL functions)
#include <epoxy/gl.h>


/*!-----------------------------------------------------------------------------------------------
 * \class   MyGLArea
 * \brief   Graphical interface using GTKmm and OpenGL.
 * \details This class creates a window embding a Gtk::GLArea widget for OpenGL rendering
 */
class MyGLArea : public Gtk::Window {
    
public:
    /*! Constructor */
    MyGLArea();
    /*! Destructor */
    ~MyGLArea() override;
    
private:
    // Window elements
    Gtk::Box vbox {Gtk::ORIENTATION_VERTICAL};      ///< Main frame for organising the window
    Gtk::GLArea gl_area;                            ///< Area for OpenGL drawings
    Gtk::Box x_hbox {Gtk::ORIENTATION_HORIZONTAL};  ///< Box for gathering the 1st slider and its description
    Gtk::Label x_label {"X orientation"};           ///< Description of the 1st slider
    Gtk::Scale x_scale;                             ///< 1st slider (graphical part)
    Glib::RefPtr<Gtk::Adjustment> x_adjustment;     ///< 1st slider (value part)
    Gtk::Box y_hbox {Gtk::ORIENTATION_HORIZONTAL};  ///< Box for gathering the 2nd slider and its description
    Gtk::Label y_label {"Y orientation"};           ///< Description of the 2nd slider
    Gtk::Scale y_scale;                             ///< 2nd slider (graphical part)
    Glib::RefPtr<Gtk::Adjustment> y_adjustment;     ///< 2nd slider (value part)
    Gtk::Box z_hbox {Gtk::ORIENTATION_HORIZONTAL};  ///< Box for gathering the 3rd slider and its description
    Gtk::Label z_label {"Z orientation"};           ///< Description of the 3rd slider
    Gtk::Scale z_scale;                             ///< 3rd slider (graphical part)
    Glib::RefPtr<Gtk::Adjustment> z_adjustment;     ///< 3rd slider (value part)
    Gtk::Button quit_button {"Quit"};               ///< Push button
    
    // OpenGL elements
    unsigned int gl_vao {0}; ///< Vertex Array Object
    unsigned int gl_vbo {0}; ///< Vertex Buffer Object
//     GLuint m_EBO {0}; ///< Element Buffer Object
    unsigned int gl_texture0 {0}; ///< Texture
    Shader* shader; ///< Shader programs in GLSL
    
protected:
    /*! Initialization of buffers and shaders */
    void realize();
    /*! Clean up what's in realize */
    void unrealize();
    /*! Things to draw in there */
    bool render(const Glib::RefPtr<Gdk::GLContext>& context);
    
    /*! Initiate the OpenGL buffers (VAO & VBO) */
    void init_buffers();
    /*! Look for .glsl files, compile it into a shader program */
    void init_shaders();
    
private:
    /*! Computation of the cube orientation */
    void compute_model_pose(float *res, float phi, float theta, float psi);
    /*! Callback for updating the OpenGL render when a slider position changes */
    void on_adjustment_value_changed();
};

#endif // MY_GLAREA_H
