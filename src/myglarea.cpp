/*****************************************************************************
 * <GTKmm_OpenGL - Example of GTKmm window with OpenGL.>
 * Copyright (C) 2018  B. Véron
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

// Local includes
#include "myglarea.h"

// Standard includes
#include <iostream>
#include <string>

// GLM includes (mathematics for OpenGL)
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp>


MyGLArea::MyGLArea() {
    set_title("GL Area");
    set_default_size(400, 600);
    
    vbox.property_margin() = 12;
    vbox.set_spacing(1);
    add(vbox);
    
    gl_area.set_hexpand(true);
    gl_area.set_vexpand(true);
    
    vbox.add(gl_area);
    gl_area.show();
    // Connect GLArea signals
    gl_area.signal_realize().connect(sigc::mem_fun(*this, &MyGLArea::realize));
    // Important that the unrealize signal calls our handler to clean up
    // GL resources _before_ the default unrealize handler is called (the "false")
    gl_area.signal_unrealize().connect(sigc::mem_fun(*this, &MyGLArea::unrealize), false);
    gl_area.signal_render().connect(sigc::mem_fun(*this, &MyGLArea::render), false);
    
    x_hbox.property_margin() = 12;
    x_hbox.set_spacing(6);
    
    x_adjustment = Gtk::Adjustment::create(0.0, -180.0, 180.0, 5.0);
    x_scale.set_adjustment(x_adjustment);
    x_scale.set_hexpand(true);
    
    x_hbox.add(x_label);
    x_hbox.add(x_scale);
    vbox.add(x_hbox);
    x_scale.show();
    x_label.show();
    x_hbox.show();
    x_adjustment->signal_value_changed().connect(sigc::mem_fun(*this, &MyGLArea::on_adjustment_value_changed));
    
    y_hbox.property_margin() = 12;
    y_hbox.set_spacing(6);
    
    y_adjustment = Gtk::Adjustment::create(0.0, -180.0, 180.0, 5.0);
    y_scale.set_adjustment(y_adjustment);
    y_scale.set_hexpand(true);
    
    y_hbox.add(y_label);
    y_hbox.add(y_scale);
    vbox.add(y_hbox);
    y_scale.show();
    y_label.show();
    y_hbox.show();
    y_adjustment->signal_value_changed().connect(sigc::mem_fun(*this, &MyGLArea::on_adjustment_value_changed));
    
    z_hbox.property_margin() = 12;
    z_hbox.set_spacing(6);
    
    z_adjustment = Gtk::Adjustment::create(0.0, -180.0, 180.0, 5.0);
    z_scale.set_adjustment(z_adjustment);
    z_scale.set_hexpand(true);
    
    z_hbox.add(z_label);
    z_hbox.add(z_scale);
    vbox.add(z_hbox);
    z_scale.show();
    z_label.show();
    z_hbox.show();
    z_adjustment->signal_value_changed().connect(sigc::mem_fun(*this, &MyGLArea::on_adjustment_value_changed));
    
    quit_button.set_hexpand(true);
    vbox.add(quit_button);
    
    // Connect clicked to close of window
    quit_button.signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Window::close));
    quit_button.show();
    
    vbox.show();
    
    shader = nullptr;
}


MyGLArea::~MyGLArea() {
    if (shader != nullptr) {
        gl_area.make_current();
        delete shader;
    }
}


void MyGLArea::realize() {
    // We need to make the context current if we want to
    // call GL API
    gl_area.make_current();
    try {
        // If there were errors during the initialization or
        // when trying to make the context current, this
        // function will return a #GError for you to catch
        gl_area.throw_if_error();
        
        // Configure global OpenGL state
        gl_area.set_has_depth_buffer(true);
        
        // Configure the buffers and shaders as you want
        init_buffers();
        init_shaders();
    }
    catch(const Gdk::GLError& gle) {
        std::cerr << "An error occured making the context current during realize:" << std::endl;
        std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
    }
}


void MyGLArea::unrealize() {
    gl_area.make_current();
    try {
        gl_area.throw_if_error();
        
        // Delete buffers and program
        glDeleteBuffers(1, &gl_vbo);
        glDeleteVertexArrays(1, &gl_vao);
    }
    catch(const Gdk::GLError& gle) {
        std::cerr << "An error occured making the context current during unrealize" << std::endl;
        std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
    }
}


bool MyGLArea::render(const Glib::RefPtr<Gdk::GLContext>& /* context */) {
    try {
        gl_area.throw_if_error();
        
        // Inside this function it's safe to use GL; the given
        // #GdkGLContext has been made current to the drawable
        // surface used by the #GtkGLArea and the viewport has
        // already been set to be the size of the allocation
        
        // We can start by clearing the buffer
        glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Activate the shader
        shader->use();
        
        // Bind textures on corresponding texture units
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gl_texture0);
        shader->setInt("texture0", 0);
        
        // Create transformations
        // Object position
        glm::mat4 model(1.0f);
        float phi = (float)x_adjustment->get_value();
        float theta = (float)y_adjustment->get_value();
        float psi = (float)z_adjustment->get_value();
        compute_model_pose(glm::value_ptr(model), phi, theta, psi);
        // Camera position
        glm::mat4 view(1.0f);
        view = glm::translate(view, glm::vec3(0.0f, 0.0f, -5.0f));
        // Projection
        glm::mat4 projection(1.0f);
        projection = glm::perspective(glm::radians(45.0f),
                                      (float)gl_area.get_width() / (float)gl_area.get_height(),
                                      0.1f, 100.0f);
        
        // Transfer the matrices to the shader program
        shader->setMatrix4("model", glm::value_ptr(model));
        shader->setMatrix4("view", glm::value_ptr(view));
        shader->setMatrix4("projection", glm::value_ptr(projection));
        
        // Get the VAO
        glBindVertexArray(gl_vao);
        glDrawArrays(GL_TRIANGLES, 0, 3*2*6);
        
        // We completed our drawing; the draw commands will be
        // flushed at the end of the signal emission chain, and
        // the buffers will be drawn on the window
        glFlush();
        return true;
    }
    catch(const Gdk::GLError& gle) {
        std::cerr << "An error occurred in the render callback of the GLArea" << std::endl;
        std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
        return false;
    }
}


void MyGLArea::init_buffers() {
    float a = 1.0;
    float b = 1.0;
    float c = 1.0;
    const float vertices[] = {
//      positions   colors         textures
        -a, -b, -c, 1.0, 0.0, 0.0, 1.0, 0.0, // back
         a, -b, -c, 1.0, 0.0, 0.0, 0.0, 0.0,
        -a,  b, -c, 1.0, 0.0, 0.0, 1.0, 1.0,
         a,  b, -c, 1.0, 0.0, 0.0, 0.0, 1.0,
         a, -b, -c, 1.0, 0.0, 0.0, 0.0, 0.0,
        -a,  b, -c, 1.0, 0.0, 0.0, 1.0, 1.0,
        -a, -b, -c, 0.0, 1.0, 0.0, 0.0, 0.0, // left
        -a,  b, -c, 0.0, 1.0, 0.0, 0.0, 1.0,
        -a, -b,  c, 0.0, 1.0, 0.0, 1.0, 0.0,
        -a,  b, -c, 0.0, 1.0, 0.0, 0.0, 1.0,
        -a, -b,  c, 0.0, 1.0, 0.0, 1.0, 0.0,
        -a,  b,  c, 0.0, 1.0, 0.0, 1.0, 1.0,
        -a, -b, -c, 0.0, 0.0, 1.0, 0.0, 0.0, // bottom
         a, -b, -c, 0.0, 0.0, 1.0, 1.0, 0.0,
        -a, -b,  c, 0.0, 0.0, 1.0, 0.0, 1.0,
         a, -b, -c, 0.0, 0.0, 1.0, 1.0, 0.0,
        -a, -b,  c, 0.0, 0.0, 1.0, 0.0, 1.0,
         a, -b,  c, 0.0, 0.0, 1.0, 1.0, 1.0,
        -a, -b,  c, 1.0, 1.0, 0.0, 0.0, 0.0, // front
         a, -b,  c, 1.0, 1.0, 0.0, 1.0, 0.0,
        -a,  b,  c, 1.0, 1.0, 0.0, 0.0, 1.0,
         a,  b,  c, 1.0, 1.0, 0.0, 1.0, 1.0,
         a, -b,  c, 1.0, 1.0, 0.0, 1.0, 0.0,
        -a,  b,  c, 1.0, 1.0, 0.0, 0.0, 1.0,
        -a,  b, -c, 0.0, 1.0, 1.0, 0.0, 1.0, // top
         a,  b, -c, 0.0, 1.0, 1.0, 1.0, 1.0,
        -a,  b,  c, 0.0, 1.0, 1.0, 0.0, 0.0,
         a,  b, -c, 0.0, 1.0, 1.0, 1.0, 1.0,
        -a,  b,  c, 0.0, 1.0, 1.0, 0.0, 0.0,
         a,  b,  c, 0.0, 1.0, 1.0, 1.0, 0.0,
         a, -b, -c, 1.0, 0.0, 1.0, 1.0, 0.0, // right
         a,  b, -c, 1.0, 0.0, 1.0, 1.0, 1.0,
         a, -b,  c, 1.0, 0.0, 1.0, 0.0, 0.0,
         a,  b, -c, 1.0, 0.0, 1.0, 1.0, 1.0,
         a, -b,  c, 1.0, 0.0, 1.0, 0.0, 0.0,
         a,  b,  c, 1.0, 0.0, 1.0, 0.0, 1.0
    };
    
    glGenVertexArrays(1, &gl_vao); // Create the VAO
    glBindVertexArray(gl_vao); // Bind the VAO to use it
    
    glGenBuffers(1, &gl_vbo); // Generate the VBO
    glBindBuffer(GL_ARRAY_BUFFER, gl_vbo); // Bind the VBO
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // Put the data in the VBO
    // Define the attributes
    // Position attribute
    glVertexAttribPointer(
        0,                  // nb of the attribute
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        8*sizeof(float),    // stride, if 0 OpenGL can guess the stride
        (void*)0            // array buffer offset
    );
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    // Texture coords attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Disactivate the VBO
    
    // Generate texture
    glGenTextures(1, &gl_texture0);
    glBindTexture(GL_TEXTURE_2D, gl_texture0);
    // Set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image
    std::string face_image_path = "./resources/gtk.png";
    Glib::RefPtr<Gdk::Pixbuf> face_texture = Gdk::Pixbuf::create_from_file(face_image_path);
    face_texture = face_texture->flip(false);
    // Generate the texture
    // Note that the png image has transparency and thus an alpha channel,
    // so make sure to tell OpenGL the data type is of GL_RGBA
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, face_texture->get_width(), face_texture->get_height(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, face_texture->get_pixels());
    // Generate mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    // Enable blending
//     glEnable(GL_BLEND);
//     glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    /* NOTE: for blending to show transparency properly, objects should be drawn
     * in a specific order (from the back of the scene to the front).
     * Here, a simpler solution is choosen to handle transparency : the color is
     * disabled if its alpha channel is lower than a threashold (cf. fragment shader).
     * Drawback : drawing semi-transparent objects is not possible.
     */
    
    glBindVertexArray(0);
}


void MyGLArea::init_shaders() {
    // Get the Vertex Shader file path
    const char* vertex_file_path = "./resources/vertexShader.glsl";
    // Get the Fragment Shader file path
    const char* fragment_file_path = "./resources/fragmentShader.glsl";
    
    shader = new Shader(vertex_file_path, fragment_file_path);
}


void MyGLArea::compute_model_pose(float *res, float  phi, float  theta, float  psi) {
    float x = phi * (G_PI / 180.f);
    float y = theta * (G_PI / 180.f);
    float z = psi * (G_PI / 180.f);
    float c1 = cosf (x), s1 = sinf (x);
    float c2 = cosf (y), s2 = sinf (y);
    float c3 = cosf (z), s3 = sinf (z);
    float c3c2 = c3 * c2;
    float s3c1 = s3 * c1;
    float c3s2s1 = c3 * s2 * s1;
    float s3s1 = s3 * s1;
    float c3s2c1 = c3 * s2 * c1;
    float s3c2 = s3 * c2;
    float c3c1 = c3 * c1;
    float s3s2s1 = s3 * s2 * s1;
    float c3s1 = c3 * s1;
    float s3s2c1 = s3 * s2 * c1;
    float c2s1 = c2 * s1;
    float c2c1 = c2 * c1;
    /* apply all three Euler angles rotations using the three matrices:
     * 
     * ⎡  c3 s3 0 ⎤ ⎡ c2  0 -s2 ⎤ ⎡ 1   0  0 ⎤
     * ⎢ -s3 c3 0 ⎥ ⎢  0  1   0 ⎥ ⎢ 0  c1 s1 ⎥
     * ⎣   0  0 1 ⎦ ⎣ s2  0  c2 ⎦ ⎣ 0 -s1 c1 ⎦
     */
    res[0] = c3c2;  res[4] = s3c1 + c3s2s1; res[8] = s3s1 - c3s2c1; res[12] = 0.f;
    res[1] = -s3c2; res[5] = c3c1 - s3s2s1; res[9] = c3s1 + s3s2c1; res[13] = 0.f;
    res[2] = s2;    res[6] = -c2s1;         res[10] = c2c1;         res[14] = 0.f;
    res[3] = 0.f;   res[7] = 0.f;           res[11] = 0.f;          res[15] = 1.f;
}


void MyGLArea::on_adjustment_value_changed() {
    gl_area.queue_render();
}
