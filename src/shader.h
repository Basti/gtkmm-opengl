/*****************************************************************************
 * <GTKmm_OpenGL - Example of GTKmm window with OpenGL.>
 * Copyright (C) 2018  B. Véron
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef SHADER_H
#define SHADER_H

// Standard includes
#include <string>

/*!-----------------------------------------------------------------------------------------------
 * \class   Shader
 * \brief   Holder for GLSL programs.
 * \details This class provides tools for holding, compiling and using OpenGL shader programs
 *          written in GLSL.
 */
class Shader {

public:
    /*! Constructor */
    Shader(const char* vertexPath, const char* fragmentPath);
    /*! Destructor */
    virtual ~Shader();
    
    /*! Use/activate the shader. */
    void use();
    
    // --------------------------------------------------------------------------------
    /// \name       Utility uniform functions
    /// @{
    /*! Transfer an int to the shader. */
    void setInt(const std::string &name, int value) const;
    /*! Transfer a 4x4 matrix to the shader. */
    void setMatrix4(const std::string &name, float* value) const;
    /// @}

private:
    /*! Compile a shader program. */
    unsigned int compileShader(int type, const char* src);
    
private:
    unsigned int programId; ///< The program id.
};

#endif // SHADER_H
