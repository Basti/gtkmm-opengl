/*****************************************************************************
 * <GTKmm_OpenGL - Example of GTKmm window with OpenGL.>
 * Copyright (C) 2018  B. Véron
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

// Local includes
#include "shader.h"

// Standard includes
#include <fstream>
#include <sstream>
#include <iostream>

// Epoxy includes (link to OpenGL functions)
#include <epoxy/gl.h>


Shader::Shader(const char* vertexPath, const char* fragmentPath) {
    // Retrieve the vertex & fragment soucre code from file path.
    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vertexShaderFile;
    std::ifstream fragmentShaderFile;
    vertexShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fragmentShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        // Open files
        vertexShaderFile.open(vertexPath);
        fragmentShaderFile.open(fragmentPath);
        std::stringstream vertexShaderStream, fragmentShaderStream;
        // Read file's buffer contents into streams
        vertexShaderStream << vertexShaderFile.rdbuf();
        fragmentShaderStream << fragmentShaderFile.rdbuf();       
        // Close file handlers
        vertexShaderFile.close();
        fragmentShaderFile.close();
        // Convert stream into string
        vertexCode   = vertexShaderStream.str();
        fragmentCode = fragmentShaderStream.str();     
    }
    catch(std::ifstream::failure e) {
        std::cerr << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    const char* vertexShaderCode = vertexCode.c_str();
    const char* fragmentShaderCode = fragmentCode.c_str();
    
    // Compile shaders
    unsigned int vertex = compileShader(GL_VERTEX_SHADER, vertexShaderCode);
    unsigned int fragment = compileShader(GL_FRAGMENT_SHADER, fragmentShaderCode);
    
    // Shader program
    programId = glCreateProgram();
    glAttachShader(programId, vertex);
    glAttachShader(programId, fragment);
    glLinkProgram(programId);
    // Print linking errors if any
    int status;
    glGetProgramiv(programId, GL_LINK_STATUS, &status);
    if(!status) {
        int log_len;
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &log_len);
        std::string log_space(log_len+1, ' ');
        glGetProgramInfoLog(programId, log_len, nullptr, (GLchar*)log_space.c_str());
        std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << log_space << std::endl;
        glDeleteProgram(programId);
        programId = 0;
    }
    else {        
        glDetachShader(programId, vertex);
        glDetachShader(programId, fragment);
    }
    
    // Delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}


Shader::~Shader() {
    glDeleteProgram(programId);
}


void Shader::use() {
    glUseProgram(programId);
}


void Shader::setInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(programId, name.c_str()), value);
}


void Shader::setMatrix4(const std::string &name, float* value) const {
    unsigned int transformLoc = glGetUniformLocation(programId, name.c_str());
    glUniformMatrix4fv(transformLoc, 1, GL_FALSE, value);
}


unsigned int Shader::compileShader(int type, const char* src) {
    auto shader = glCreateShader(type);
    glShaderSource(shader, 1, &src, nullptr);
    glCompileShader(shader);
    
    int status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE) {
        int log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
        
        std::string log_space(log_len+1, ' ');
        glGetShaderInfoLog(shader, log_len, nullptr, (GLchar*)log_space.c_str());
        
        std::cerr << "Compile failure in " <<
        (type == GL_VERTEX_SHADER ? "vertex" : "fragment") <<
        " shader: " << log_space << std::endl;
        
        glDeleteShader(shader);

        return 0;
    }

    return shader;
}
